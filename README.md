# config-json

A simple class for accessing JSON configuration files.

Use it to store all your app's configuration in replaceable JSON files. My
personal use case is a system where multiple instances of the same app run with
different configurations.

Note that `require('config-json')` will always return the same object. The idea
here is that you only have to load data in one place without having to handle
the same things elsewhere.

config-json will store data from multiple files simultaneously and lets you
switch between them.

## Installation

	npm install config-json

(Use `npm install config-json --save` to automatically add to the dependencies
list in your `package.json` file.)

## Usage

Basic example:

	/*
	Your example data.json file might look like this:
	{
		"key": {
			"subkey": "An important value"
		},
		"key2": 500
	}
	*/

	// Require the module, as usual.
	var config = require('config-json');

	// Load data from a JSON file.
	config.load('./data.json');	

	// Retrieve data from that file
	var myValue = config.get('key', 'subkey');

`.get` takes as many parameters as you want. You can drill down to any
`level of a JSON file that way.

	var entire_file_contents = config.get();
	var one_level_down = config.get('L1');
	var two_levels_down = config.get('L1', 'L2');

You can also pass in an array of keys:

	var the_code = ['UP', 'UP', 'DOWN', 'DOWN', 'LEFT', 'RIGHT', 'LEFT', 'RIGHT', 'B', 'A'];
	var cheat = config.get(the_code);

You can load data from a JSON file, or you can pass in a key (we'll get to that
in a moment) and an object of any sort yourself:

	// Load data from a file
	config.load('datafile');

	// Load data from existing object
	var myData = { ... };
	config.load('important_things', myData);

Use `.setBaseDir` to pre-set the directory where your config files live. That way
you need only specify the file name for loading each file:

	config.setBaseDir('./my_data_dir');

	config.load('file1');
	config.load('file2');
	config.load('file3');

I mentioned a "key" above. When using `.load` multiple times, the data is stored
separately. When loading from files, the file argument you passed in is the key.
When loading data from an existing object, the key is manually set.

By default the last `.load` defines which object is currently being used. Using
`.setKey` you can switch between the data objects.

	config.load('campbell');
	config.load('algar');

	var firstName = config.get('first_name'); // -> Garth
	config.setKey('campbell');
	firstName = config.get('first_name'); // -> Wayne

When using `.get` to fetch a key that does not exist, `undefined` is returned by
default. No error is raised. Note that this happens regardless of whether the
first or last key is missing.

By using `.setDefault` you can override the fallback return value.

	var value = config.get('does', 'not', 'compute'); // -> undefined
	config.setDefault("I'm sorry Dave, I'm afraid I can't do that");
	var value = config.get('does', 'not', 'compute'); // -> "I'm sorry Dave, I'm afraid I can't do that"

Using `clearCache()` will delete all loaded data and invalidate Node's require cache. Useful for when the files have changed.

	config.clearCache();

Using `.createInstance()` you can create a separate config instance that will not be coupled.

	var Config = require('config-json');
	var config_one = Config.createInstance.load('./data/file1.json');
	var config_two = Config.createInstance.load('./data/file2.json');

## Plans

Provide a separate interface object for each data file/object. Right now setKey
will set the current key globally which may not be the desired effect.
