var path = require('path');

var ConfigJSON = function(copySettings) {
	this.reset();
	if('object'==typeof copySettings) {
		this.data = copySettings.data;
		this.baseDir = copySettings.baseDir;
		this.key = copySettings.key;
		this.defaultValue = copySettings.data;
	}
	return this;
};

ConfigJSON.prototype.reset = function() {
	this.data = {};
	this.baseDir = null;
	this.key = null;
	this.defaultValue = undefined;
	this.requiredFiles = [];
};

ConfigJSON.prototype.setBaseDir = function(baseDir) {
	this.baseDir = baseDir;
};

ConfigJSON.prototype.getBaseDir = function() {
	return this.baseDir;
};

ConfigJSON.prototype.load = function(key, data) {
	if('undefined'==typeof data) {
		var filePath = path.resolve(this.baseDir ? path.join(this.baseDir, key) : key);
		this.data[key] = require(filePath);
		this.requiredFiles.push(filePath);
	} else {
		this.data[key] = data;
	}
	this.setKey(key);
};

ConfigJSON.prototype.isLoaded = function(key) {
	return 'undefined'!==typeof this.data[key];
};

ConfigJSON.prototype.getData = function() {
	return this.data;
};

ConfigJSON.prototype.setKey = function(key) {
	this.key = key;
};

ConfigJSON.prototype.getKey = function() {
	return this.key;
};

ConfigJSON.prototype.setDefaultValue = function(defaultValue) {
	this.defaultValue = defaultValue;
};

ConfigJSON.prototype.getDefaultValue = function() {
	return this.defaultValue;
};

ConfigJSON.prototype.get = function() {
	return this.getForDataKey(this.key, arguments);
};

ConfigJSON.prototype.getForDataKey = function() {
	var i, k,
		keys = this.flattenArgs(arguments),
		dataKey = keys.shift();
		ret = this.data[dataKey];
	while(keys.length>0) {
		k = keys.shift();
		if(k in ret) {
			ret = ret[k];
		} else {
			return this.defaultValue;
		}
	}
	return ret;
};

ConfigJSON.prototype.flattenArgs = function(args) {
	var a, arg, flat = [];
	for(i in args) {
		arg = args[i];
		if(typeof(arg)!='object') {
			flat.push(arg);
		} else {
			for(a in arg) {
				flat.push(arg[a]);
			}
		}
	}
	return flat;
};

ConfigJSON.prototype.clearCache = function() {
	for(var i in this.requiredFiles) {
		delete require.cache[require.resolve(this.requiredFiles[i])];
	}
	this.data = [];
	this.requiredFiles = [];
};

ConfigJSON.prototype.createInstance = function(copySettings) {
	return new ConfigJSON(copySettings);
};

module.exports = new ConfigJSON();
