var expect = require('expect.js'),
	config = require('..');

describe('base object and functions', function() {
	it('should expose an object', function() {
		expect(config).to.be.a('object');
	});

	it('should have a function setBaseDir', function() {
		expect(config.setBaseDir).to.be.a('function');
	});

	it('should have a function load', function() {
		expect(config.load).to.be.a('function');
	});

	it('should have a function setKey', function() {
		expect(config.setKey).to.be.a('function');
	});

	it('should have a function setDefault', function() {
		expect(config.setDefault).to.be.a('function');
	});

	it('should have a function get', function() {
		expect(config.get).to.be.a('function');
	});

	it('should have a function reset', function() {
		expect(config.reset).to.be.a('function');
	});
});

describe('default values', function() {
	it('data', function() {
		config.reset();
		var data = config.getData();
		expect(data).to.eql({});
	});

	it('baseDir', function() {
		config.reset();
		var baseDir = config.getBaseDir();
		expect(baseDir).to.equal(null);
	});

	it('key', function() {
		config.reset();
		var key = config.getKey();
		expect(key).to.equal(null);
	});

	it('defaultValue', function() {
		config.reset();
		var defaultValue = config.getDefault();
		expect(defaultValue).to.be(undefined);
	});
});

describe('setter/getter functions', function() {
	config.reset();

	it('should remember the baseDir', function() {
		config.setBaseDir('./data');
		var baseDir = config.getBaseDir();
		expect(baseDir).to.equal('./data');
	});

	it('should remember the key', function() {
		var key = config.getKey();
		expect(key).to.equal(null);
		config.setKey('myKey');
		var key = config.getKey();
		expect(key).to.equal('myKey');
	});

	it('should remember the default value', function() {
		config.setDefault('test value');
		var defaultValue = config.getDefault();
		expect(defaultValue).to.equal('test value');
	});
});

describe('load', function() {
	it('should load data from a file', function() {
		config.reset();
		var dataPath = './test/data/file1.json';
		config.load(dataPath);
		var data = config.getData();
		expect(data[dataPath]['what file is this']).to.be('file1');
	});

	it('should load data from a second file', function() {
		var dataPath = './test/data/file2.json';
		config.load(dataPath);
		var data = config.getData();
		expect(data[dataPath]['what file is this']).to.be('file2');
	});

	it('should then be able to switch back to the first file', function() {
		var dataPath = './test/data/file1.json';
		config.setKey(dataPath);
		var data = config.getData();
		expect(data[dataPath]['what file is this']).to.be('file1');
	});

	it('should find a file when using baseDir', function() {
		config.reset();
		var dataDir = './test/data';
		var dataFile = 'file1.json';
		config.setBaseDir(dataDir);
		config.load(dataFile);
		var data = config.getData();
		expect(data[dataFile]['what file is this']).to.be('file1');
	});
});

describe('get', function() {
	config.reset();
	config.load('./test/data/file1.json');

	it('should find a value', function() {
		var val = config.get('what file is this');
		expect(val).to.be('file1');
	});

	it('should return the default value when an incorrect key is given', function() {
		var val = config.get('what year is this');
		expect(val).to.be(undefined);
	});
});