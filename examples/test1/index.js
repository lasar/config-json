var config = require('../../');

config.setBaseDir('.');
config.load('./data.json');

console.log(config.get('some', 'thing'));

console.log(config.get('another'));

console.log(config.get('does', 'not', 'compute'));