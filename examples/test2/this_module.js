var config = require('../../');

var ThisModule = function() {};

ThisModule.prototype.anybodyOutThere = function() {
	return config.get('some', 'thing');
};

module.exports = ThisModule;