var config = require('../../');

config.setBaseDir('./data');
config.load('testdata');

console.log(config.get('some', 'thing'));

var ThatModule = require('./this_module'),
	thatModule = new ThatModule();

console.log(thatModule.anybodyOutThere());
